import Button from './Button';
import useColorSwitch from './useColorSwitch';

function App() {
  const [color, handleClick1]=useColorSwitch()
  const [color2, handleClick2]=useColorSwitch("#0000ff", "#ff00ff")
  return(
    <div>
      <Button width="120px" label="按钮" onClick={handleClick1}>
        <span>&gt;</span>
      </Button>
      <p style={{color}}>这是一段文本</p>
      <Button label="点我" onClick={handleClick2}/>
      <p style={{color: color2}}>这是第二段文本</p>
    </div>
  ) 
}

export default App;
