import styled, { css } from 'styled-components'

const cricleMixinFunc = (color,size="8px") => css`
    content: "";
    display: block;
    position: absolute;
    width: ${size};
    height: ${size};
    border-radius: 50%;
    background-color: ${color};      
`;

const StyledAvatar = styled.div`
    position: relative;
`
const StatusIcon = styled.div`
    position: absolute;
    left: 2px;
    top: 4px;

    /* &表示外层div本身，也可以省略 */
    &::before{
        ${({size})=>cricleMixinFunc("white",size)}

        transform: scale(2);
    }

    &::after{
        /* 箭头函数，不要忘记写return！！！ */
        ${({theme, status, size})=>{
            if(status==="online"){
                return cricleMixinFunc(theme.green, size)
            }else if(status==="offline"){
                return cricleMixinFunc(theme.gray, size)
            }
        }}       
    }
`
const AvatorClip = styled.div`
    width: ${({size})=>size};
    height: ${({size})=>size};
    border-radius: 50%;
    overflow: hidden;
`
const AvatorImage = styled.img`
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: center;
`

export default StyledAvatar;
export {StatusIcon, AvatorClip, AvatorImage}